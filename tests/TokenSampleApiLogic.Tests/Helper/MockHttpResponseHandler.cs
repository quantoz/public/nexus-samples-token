﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TokenSampleApiLogic.Tests.Helper
{
    public class MockHttpResponseHandler : DelegatingHandler
    {
        public class Request : IEquatable<Request>
        {
            public string Uri { get; set; }
            public string Content { get; set; }
            public HttpMethod Method { get; set; }

            public Request(HttpRequestMessage r)
            {
                Content = r?.Content?.ReadAsStringAsync().Result;
                Uri = r?.RequestUri?.ToString();
                Method = r?.Method;
            }

            bool IEquatable<Request>.Equals(Request other)
            {
                return Uri == other.Uri && Content == other.Content && Method == other.Method;
            }

            public override int GetHashCode()
            {
                // Constant because equals tests mutable member.
                // This will give poor hash performance, but will prevent bugs.
                return 0;
            }
        }

        private readonly Dictionary<Request, HttpResponseMessage> _mockResponses = new Dictionary<Request, HttpResponseMessage>();

        public void AddMockResponse(HttpRequestMessage requestMessage, HttpResponseMessage responseMessage)
        {
            _mockResponses.Add(new Request(requestMessage), responseMessage);
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage requestMessage, System.Threading.CancellationToken cancellationToken)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var request = new Request(requestMessage);
            if (_mockResponses.ContainsKey(request))
            {
                return _mockResponses[request];
            }
            else
            {
                // throw new ArgumentException("FakeRequest not registered");
                // commmented this line because here we are mocking horizon responses and we do not need to mock exact responses every time.
                // we should come up with a better solution to mock horizon responses.
                return _mockResponses.Values.First();
            }
        }

    }
}
