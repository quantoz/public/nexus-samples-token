using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using FluentAssertions;
using Xunit;
using TokenSampleApi.Data;
using TokenSampleApi.Logic.Interfaces;
using TokenSampleApi.Models;
using TokenSampleApiLogic.Tests.Helper;
using TokenSampleApi.Data.DTOs.Repo.Customers;

namespace TokenSampleApiLogic.Tests
{
    public class CustomerLogicTests
    {
        [Fact]
        public async void CreateCustomer_Success()
        {
            async Task Seed(ApplicationDbContext context, UserManager<ApplicationUser> userMgr) { }

            static async Task Run(ApplicationDbContext context, ICustomerLogic customerLogic, UserManager<ApplicationUser> userMgr)
            {
                Func<Task> act = async () => await customerLogic.Create(new CreateCustomerDto()
                {
                    Email = "user@test.com",
                    Password = "1234abCD!"
                });

                act.Should().NotThrow();

                // beware, email is used as Id Name
                userMgr.FindByNameAsync("user@test.com").Result.Should().NotBeNull();
            }

            await ExecuteTest(Seed, Run);
        }

        [Fact]
        public async void CreateCustomer_User_Exists_Failure()
        {
            async Task Seed(ApplicationDbContext context, UserManager<ApplicationUser> userMgr)
            {
                await userMgr.CreateAsync(new ApplicationUser(){ UserName = "user@test.com"});
            }

            static async Task Run(ApplicationDbContext context, ICustomerLogic customerLogic, UserManager<ApplicationUser> userMgr)
            {
                Func<Task> act = async () => await customerLogic.Create(new CreateCustomerDto()
                {
                    Email = "user@test.com",
                    Password = "1234abCD!"
                });

                act.Should().Throw<ArgumentException>()
                    .Where(e => e.Message.StartsWith("User with this Email exists"));
            }

            await ExecuteTest(Seed, Run);
        }

        private async Task ExecuteTest(Func<ApplicationDbContext, UserManager<ApplicationUser>, Task> seed, Func<ApplicationDbContext, ICustomerLogic, UserManager<ApplicationUser>, Task> run,
            MockHttpResponseHandler responseHandler = null)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlite(connection));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                // Ensure that the context was correctly created.
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                // Use serviceProvider to get the UserManager
                using (var serviceProvider = services.BuildServiceProvider())
                {
                    using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                    {
                        var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                        // Use a context instance to seed the database before the test run.
                        using (var context = new ApplicationDbContext(options))
                        {
                            if (seed != null)
                            {
                                await seed(context, userMgr);
                            }
                        }

                        // Use a context instance to run the test
                        using (var context = new ApplicationDbContext(options))
                        {
                            //var customerLogic = new CustomerLogic(userMgr);
                            //await run(context, customerLogic, userMgr);
                        }
                    }
                }
            }
            finally
            {
                // Close the database connection
                connection.Close();
            }
        }
    }
}
