# Quantoz Mobile Token Sample Application

## Intallation

1. `yarn install`
1. `rm -v node_modules/*/.babelrc`

### iOS

1. `cd ios && pod install`
1. `yarn ios`
1. **OR**

- run `yarn build:ios`
- Open the `ios/SampleApp.xcworkspace` project and build in xcode.

### Android

1. `cd android && ./gradlew clean`
1. start metro server: `yarn start`
1. start log reading server: `yarn log-android`
1. run app and deploy to emulator: `yarn android`

### Reset whole environment if needed

1. `git clean -dfx`
1. `rm -rf node_modules && yarn`
1. `rm -v node_modules/*/.babelrc`
1. `rm -rf $TMPDIR/haste-map-react-*`
1. `rm -rf $TMPDIR/metro-cache/*`

## Common Errors

### Strange error after moving code

When strange errors happen when code is moved or removed, there is probably a javascript syntax
error somewhere. Commit all changes to automatically run the javascript linter tool. This might solve the problem.

### React red screen on phone

[Reset environment](#reset-whole-environment-if-needed) after `Unable to resolve module x` or `Super expression must either be null or a function` errors.

### Metro is not starting because of a regex issue

This is fixed in a more recent metro version, however we are still using react v0.60.6, so this issue is still present.

You have to make change in this file `/node_modules/metro-config/src/defaults/blacklist.js`
There is an invalid regular expression that needed changed. Changed the first expression under sharedBlacklist from:

```javascript
var sharedBlacklist = [
  /node_modules[/\\]react[/\\]dist[/\\].*/,
  /website\/node_modules\/.*/,
  /heapCapture\/bundle\.js/,
  /.*\/__tests__\/.*/,
];
```

to

```javascript
var sharedBlacklist = [
  /node_modules[\/\\]react[\/\\]dist[\/\\].*/,
  /website\/node_modules\/.*/,
  /heapCapture\/bundle\.js/,
  /.*\/__tests__\/.*/,
];
```

## Running tests (not maintained)

`yarn test`
only specific tests:
`yarn test LoginScreen`

## Packaging

1. Create a keystore file

```bash
cd android/app
keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
```

1. Configure the password in `android/gradle.properties`
1. Generate the release APK: `cd android && ./gradlew assembleRelease`
1. Find it at `nexus-sample-app\SampleApp\android\app\build\outputs\apk\release`

## Enable Viewer mode

The app has an Account Viewer mode. This mode allows for only viewing account transactions for demo purposes.
When in viewing mode, you are not able to do any payments/cashouts.

You can enable viewer mode by going to the "Sign-Up" screen and fill in "viewer" in the email and 1st password field.
Do NOT fill in the second password field. Then click Sign-Up and you will get a QR scanner to scan the account to view.

## Possible Development Environment

- Make sure you can run using HyperV on Windows to use Android Emulator
  - you might need to configure the bootloader switch: https://www.hanselman.com/blog/SwitchEasilyBetweenVirtualBoxAndHyperVWithABCDEditBootEntryInWindows81.aspx
  - if you get "command not found error" use command prompt to execute the commando's
- Install Android Studio: https://developer.android.com/studio
- Use Android Studio SDK manager to install SDK's: 5.0 to 9.0
