import "./shim";
import { AppRegistry, Platform } from "react-native";
import App from "./src";

AppRegistry.registerComponent("SampleApp", () => App);

if (Platform.OS === "web") {
  const rootTag =
    document.getElementById("root") || document.getElementById("main");
  AppRegistry.runApplication("SampleApp", { rootTag });
}
