import React, { useEffect } from "react";
import { Platform, StatusBar } from "react-native";
import { StyleProvider, Container } from "native-base";
import getTheme from "_nativeBaseTheme/components";
import platformTheme from "_nativeBaseTheme/variables/platform";
import AppNavigator from "_navigation/AppNavigator";
import SplashScreen from "react-native-splash-screen";

export default function App(props) {
  useEffect(() => {
    Platform.OS !== "ios" && SplashScreen.hide();
  }, []);

  return (
    <StyleProvider style={getTheme(platformTheme)}>
      <Container>
        {Platform.OS === "ios" && <StatusBar barStyle="default" />}
        <AppNavigator />
      </Container>
    </StyleProvider>
  );
}
