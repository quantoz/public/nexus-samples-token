"use strict";

import React, { Component } from "react";

import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Text } from "native-base";

// import QRCodeScanner from "react-native-qrcode-scanner";
import { RNCamera } from "react-native-camera";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 20
  }
});

export default function ScanScreen({ navigation }) {
  const onScan = navigation.getParam("onScan", () => {});

  return (
    <View style={styles.container}>
      <RNCamera
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
        captureAudio={false}
        flashMode={RNCamera.Constants.FlashMode.on}
        androidCameraPermissionOptions={{
          title: "Permission to use camera",
          message: "We need your permission to use your camera",
          buttonPositive: "Ok",
          buttonNegative: "Cancel"
        }}
        barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
        onBarCodeRead={({ data }) => {
          onScan(data);
          navigation.goBack();
        }}
      />
    </View>
  );
}
