import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, ImageBackground, Image } from "react-native";
import { Container, Form, Button, Label, Text, Input, Item } from "native-base";
import { Row, Grid } from "react-native-easy-grid";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { logInAsync } from "_services/Authorization";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import {
  isUserLoggedInAsync,
  getLoggedInUserEmailAsync,
  getUserAsync,
  noStoredUsers
} from "_services/Storage";
import { ErrorModal } from "_components/Alert";
import OTPInput from "_components/OTPInput";

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  noLeftMargin: {
    marginLeft: 0
  }
});

export default function LogInScreen({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [resetPins, setResetPins] = useState(false);

  useEffect(() => {
    setResetPins(false);
  }, [password]);

  useEffect(() => {
    // skip the log-in screen, if there are no users in the local storage
    const checkIfFirstUserLogIn = async () => {
      const navToSignUp = () => {
        navigation.navigate("SignUpScreen", { email, password });
      };

      if (await noStoredUsers()) {
        ErrorModal(
          "You need an account to make full use of this application. Please sign up!",
          navToSignUp
        );
      }
    };
    checkIfFirstUserLogIn();
  }, []);

  _logInAsync = async () => {
    var login = async () => {
      try {
        await logInAsync(email.toLowerCase(), password);

        if (await isUserLoggedInAsync()) {
          var userEmail = await getLoggedInUserEmailAsync();
          var user = await getUserAsync(userEmail);
          navigation.navigate("HomeScreen", { user });
        } else {
          ErrorModal("Log-in Failed, try again");
        }
      } catch (error) {
        ErrorModal(error);
        setResetPins(true);
      }
    };

    await trackPromise(login(), "log-in-button");
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <ImageBackground
          source={require("../assets/images/background_image_onboarding.png")}
          style={styles.backgroundImage}
        >
          <Grid>
            <KeyboardAwareScrollView>
              <Row
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 10,
                  flex: 1
                }}
              >
                <Image
                  source={require("../assets/images/quantoz_logo_white.png")}
                />
                <Text notification-dark style={{ marginTop: 24 }}>
                  Token sample app
                </Text>
              </Row>
              <Row style={{ flexDirection: "column", padding: 16, flex: 2 }}>
                <Form>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>E-mail</Label>
                    <Input
                      value={email}
                      onChangeText={setEmail}
                      returnKeyType={"next"}
                      getRef={c => (this.email = c)}
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <OTPInput
                    savePass={setPassword}
                    label="Pin"
                    reset={resetPins}
                  />
                </Form>
              </Row>

              <Row
                style={{
                  flexDirection: "column",
                  justifyContent: "flex-end",
                  flex: 1,
                  padding: 16
                }}
              >
                <LoadingSpinner area="log-in-button">
                  <Button
                    block
                    primary-dark
                    style={{ marginBottom: 16 }}
                    onPress={() => {
                      _logInAsync();
                    }}
                  >
                    <Text>Log In</Text>
                  </Button>
                </LoadingSpinner>
                <Button
                  block
                  secondary-dark
                  onPress={() => {
                    navigation.navigate("SignUpScreen", { email, password });
                  }}
                >
                  <Text primary>Sign up</Text>
                </Button>
              </Row>
            </KeyboardAwareScrollView>
          </Grid>
        </ImageBackground>
      </Container>
    </SafeAreaView>
  );
}

LogInScreen.navigationOptions = {
  title: "Login",
  headerBackTitle: null
};
