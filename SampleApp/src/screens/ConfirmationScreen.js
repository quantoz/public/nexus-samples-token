import React, { useEffect, useState } from "react";
import {
  Animated,
  Easing,
  StyleSheet,
  SafeAreaView,
  ImageBackground
} from "react-native";
import { View, Text, Icon, Button, Container, Content } from "native-base";

export default function ConfirmationScreen({ navigation }) {
  const styles = StyleSheet.create({
    backgroundImage: {
      width: "100%",
      height: "100%",
      resizeMode: "cover"
    },
    statusContainer: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    circle: {
      position: "absolute",
      backgroundColor: "#F5F7FA",
      width: 0,
      height: 0
    },
    circle__small: {
      borderRadius: 85,
      opacity: 0.3
    },
    circle__medium: {
      borderRadius: 160,
      opacity: 0.2
    },
    circle__big: {
      borderRadius: 275,
      opacity: 0.1
    },
    successIcon: {
      fontSize: 120,
      color: "#F5F7FA",
      marginTop: 64,
      marginBottom: 32
    },
    buttonContainer: {
      padding: 16
    }
  });

  const [smallCircle] = useState(new Animated.Value(0));
  const [mediumCircle] = useState(new Animated.Value(0));
  const [bigCircle] = useState(new Animated.Value(0));

  const smallCircleMax = 170;
  const mediumCircleMax = 320;
  const bigCircleMax = 550;
  const animationDuration = 1000;

  useEffect(() => {
    Animated.parallel([
      Animated.timing(bigCircle, {
        toValue: bigCircleMax,
        duration: animationDuration,
        easing: Easing.bounce
      }),
      Animated.timing(mediumCircle, {
        toValue: mediumCircleMax,
        duration: animationDuration,
        delay: 100,
        easing: Easing.bounce
      }),
      Animated.timing(smallCircle, {
        toValue: smallCircleMax,
        duration: animationDuration,
        delay: 150,
        easing: Easing.bounce
      })
    ]).start();
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <ImageBackground
          source={require("../assets/images/background_image_onboarding.png")}
          style={styles.backgroundImage}
        >
          <View style={styles.statusContainer}>
            <Animated.View
              style={[
                styles.circle,
                styles.circle__big,
                { width: bigCircle, height: bigCircle }
              ]}
            />
            <Animated.View
              style={[
                styles.circle,
                styles.circle__medium,
                { width: mediumCircle, height: mediumCircle }
              ]}
            />
            <Animated.View
              style={[
                styles.circle,
                styles.circle__small,
                { width: smallCircle, height: smallCircle }
              ]}
            />
            <Icon name="checkmark" style={styles.successIcon} />
            <Text notification-dark>
              {navigation.getParam("message", "Empty message")}
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <Button
              block
              primary-dark
              onPress={() => {
                navigation.navigate(
                  navigation.getParam("destinationScreen", "LogInScreen"),
                  navigation.getParam("destinationProps", {})
                );
              }}
            >
              <Text button>{navigation.getParam("buttonText", "Done")}</Text>
            </Button>
          </View>
        </ImageBackground>
      </Container>
    </SafeAreaView>
  );
}
