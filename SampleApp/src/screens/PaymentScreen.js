import React, { useState } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import {
  Header,
  Left,
  Body,
  Right,
  Title,
  Button,
  Label,
  Text,
  Input,
  Item,
  Icon,
  Container,
  Content,
  Form
} from "native-base";
import { NavigationActions } from "react-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import Config from "Config";
import { createPayment, submitTransactionAsync } from "_services/Server";
import { signTransaction } from "_services/Stellar";
import { ErrorModal } from "_components/Alert";

const styles = StyleSheet.create({
  noLeftMargin: {
    marginLeft: 0
  },
  lastInput: {
    marginBottom: 24
  }
});

// Broaden the scope of this function to use it in the header
var _setAddress;

export default function PaymentScreen({ navigation }) {
  const [user, setUser] = useState(navigation.getParam("user", {}));
  const [address, setAddress] = useState("");
  _setAddress = setAddress;
  const [memo, setMemo] = useState("");
  const [amount, setAmount] = useState("");
  const [token, setToken] = useState(Config.DEFAULT_ASSET);

  const _createPaymentAsync = async () => {
    const doCreatePayment = async () => {
      try {
        var response = await createPayment(user, {
          fromAddress: user.keypair.publicKey,
          toAddress: address,
          tokenCode: Config.DEFAULT_ASSET,
          amount,
          memo
        });

        // response content
        // "signingNeeded": true,
        // "transactionEnvelope": ""

        console.log("start submit");
        const signedTx = signTransaction(
          response.transactionEnvelope,
          user.keypair.secret
        );
        console.log("Submitting tx:", signedTx);
        await submitTransactionAsync(user, signedTx);

        navigation.navigate("PaymentConfirmed", {
          message: "Payment Succeeded",
          destinationScreen: "HomeScreen",
          destinationProps: { user }
        });
      } catch (error) {
        ErrorModal(error);
      }
    };

    await trackPromise(doCreatePayment(), "create-payment-button");
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAwareScrollView>
        <Container>
          <Content padder>
            <Text body-light>
              Use the Scan QR code feature at the top of the screen to
              seamlessly scan the recipient address and send an instant payment.
            </Text>
            <Form>
              <Item
                floatingLabel
                floatingLabel-light
                style={styles.noLeftMargin}
              >
                <Label>Address</Label>
                <Input
                  value={address}
                  onChangeText={setAddress}
                  returnKeyType={"next"}
                  getRef={c => (this.address = c)}
                  onSubmitEditing={() => this.memo._root.focus()}
                  blurOnSubmit={false}
                />
              </Item>
              <Item
                floatingLabel
                floatingLabel-light
                style={styles.noLeftMargin}
              >
                <Label>Memo</Label>
                <Input
                  value={memo}
                  onChangeText={setMemo}
                  returnKeyType={"next"}
                  getRef={c => (this.memo = c)}
                  onSubmitEditing={() => this.amount._root.focus()}
                  blurOnSubmit={false}
                />
              </Item>
              <Item
                floatingLabel
                floatingLabel-light
                style={styles.noLeftMargin}
              >
                <Label>Amount</Label>
                <Input
                  value={amount}
                  onChangeText={setAmount}
                  returnKeyType={"next"}
                  getRef={c => (this.amount = c)}
                  onSubmitEditing={() => _createPaymentAsync()}
                  blurOnSubmit={true}
                  keyboardType="decimal-pad"
                />
              </Item>
              <Item
                floatingLabel
                floatingLabel-light
                style={[styles.noLeftMargin, styles.lastInput]}
              >
                <Label>Token</Label>
                <Input disabled value={token} />
              </Item>
            </Form>
            <LoadingSpinner area="create-payment-button">
              <Button
                block
                primary-light
                onPress={() => {
                  _createPaymentAsync();
                }}
              >
                <Text>Pay</Text>
              </Button>
            </LoadingSpinner>
          </Content>
        </Container>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}

PaymentScreen.navigationOptions = ({ navigation }) => ({
  header: () => (
    <Header darkTheme>
      <Left>
        <Button
          transparent
          onPress={() => navigation.dispatch(NavigationActions.back())}
        >
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Pay</Title>
      </Body>
      <Right>
        <Button
          transparent
          iconLeft
          onPress={() => {
            navigation.navigate("ScanScreen", {
              onScan: _setAddress
            });
          }}
        >
          <Icon name="qr-code" />
          <Text>Scan QR code</Text>
        </Button>
      </Right>
    </Header>
  )
});
