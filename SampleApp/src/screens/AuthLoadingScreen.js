import React, { useEffect } from "react";
import { ActivityIndicator, StatusBar, View } from "react-native";
import {
  isUserLoggedInAsync,
  getLoggedInUserEmailAsync,
  getUserAsync
} from "_services/Storage";
import { NavigationActions } from "react-navigation";
import { ErrorModal } from "_components/Alert";

export default function AuthLoadingScreen({ navigation }) {
  useEffect(() => {
    _bootstrapAsync();
  }, []);

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    try {
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      if (await isUserLoggedInAsync()) {
        var userEmail = await getLoggedInUserEmailAsync();
        var user = await getUserAsync(userEmail);

        const passUserToTxScreen = NavigationActions.setParams({
          key: "TransactionsStack",
          params: { user }
        });
        navigation.dispatch(passUserToTxScreen);

        const passUserToSpScreen = NavigationActions.setParams({
          key: "DevicesStack",
          params: { user }
        });
        navigation.dispatch(passUserToSpScreen);

        const passUserToWdScreen = NavigationActions.setParams({
          key: "WithdrawalsStack",
          params: { user }
        });
        navigation.dispatch(passUserToWdScreen);

        const navigateHomeAction = NavigationActions.navigate({
          routeName: "HomeScreen",
          params: { user }
        });
        navigation.navigate("App", { user }, navigateHomeAction);
      } else {
        navigation.navigate("Auth");
      }
    } catch (error) {
      ErrorModal(error);
    }
  };

  // Render any loading content that you like here
  return (
    <View>
      <StatusBar barStyle="default" />
      <ActivityIndicator />
    </View>
  );
}
