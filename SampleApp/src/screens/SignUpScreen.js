import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, ImageBackground } from "react-native";
import { NavigationActions } from "react-navigation";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Right,
  Button,
  Label,
  Text,
  Input,
  Form,
  Item,
  Icon
} from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Row, Grid } from "react-native-easy-grid";
import { createAccountAsync } from "_services/Authorization";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import {
  isUserLoggedInAsync,
  getLoggedInUserEmailAsync,
  getUserAsync
} from "../services/Storage";
import { ErrorModal, InfoModal } from "../components/Alert";
import OTPInput from "_components/OTPInput";

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  noLeftMargin: {
    marginLeft: 0
  }
});

export default function SignUpScreen({ navigation }) {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [bankAccountNumber, setBankAccountNumber] = useState();
  const [password, setPassword] = useState();
  const [rePassword, setRePassword] = useState();
  const [pwEqual, setPwEqual] = useState(false);
  const [infoModalShown, setInfoModalShown] = useState(false);
  const [resetPins, setResetPins] = useState(false);

  useEffect(() => {
    let reload = true;

    if (reload) {
      setPwEqual(password === rePassword);
      setResetPins(false);
    }

    // clean-up the promise
    return () => {
      reload = false;
    };
  }, [password, rePassword]);

  _createAccountAsync = async () => {
    var createAccount = async () => {
      try {
        await createAccountAsync(
          email.toLowerCase(),
          password,
          firstName,
          lastName,
          bankAccountNumber
        );

        if (await isUserLoggedInAsync()) {
          var userEmail = await getLoggedInUserEmailAsync();
          var user = await getUserAsync(userEmail);
          navigation.navigate("SignUpConfirmed", {
            message: "Account Created",
            buttonText: "Continue",
            destinationScreen: "HomeScreen",
            destinationProps: { user }
          });
        } else {
          ErrorModal("Create Account and log-in Failed, try again");
        }
      } catch (error) {
        ErrorModal(error);
      }
    };

    await trackPromise(createAccount(), "create-account-button");
  };

  const _startViewerMode = async () => {
    if (
      email.toLowerCase() === "viewer" &&
      password.toLowerCase() === "viewer"
    ) {
      navigation.navigate("ViewerScanScreen", {
        onScan: viewerAddress => {
          user = {
            viewer: true,
            keypair: {
              publicKey: viewerAddress
            }
          };
          navigation.navigate("HomeScreen", { user });
        }
      });
    }
  };

  const _ifPwEqualCreateAccount = async () => {
    if (pwEqual) {
      await _createAccountAsync();
    } else {
      setResetPins(true);
      _startViewerMode();
      ErrorModal("Passwords do not match");
    }
  };

  const _showInfoModal = () => {
    if (!infoModalShown) {
      InfoModal("password");
      setInfoModalShown(true);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <ImageBackground
          source={require("../assets/images/background_image_onboarding.png")}
          style={styles.backgroundImage}
        >
          <KeyboardAwareScrollView>
            <Grid>
              <Row style={{ flexDirection: "column", padding: 16 }}>
                <Form>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>First name</Label>
                    <Input
                      value={firstName}
                      onChangeText={setFirstName}
                      autoFocus={true}
                      returnKeyType={"next"}
                      getRef={c => (this.firstName = c)}
                      onSubmitEditing={() => this.lastName._root.focus()}
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>Last name</Label>
                    <Input
                      value={lastName}
                      onChangeText={setLastName}
                      returnKeyType={"next"}
                      getRef={c => (this.lastName = c)}
                      onSubmitEditing={() => this.email._root.focus()}
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>Email Address</Label>
                    <Input
                      value={email}
                      onChangeText={setEmail}
                      returnKeyType={"next"}
                      getRef={c => (this.email = c)}
                      onSubmitEditing={() =>
                        this.bankAccountNumber._root.focus()
                      }
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>Bank account number</Label>
                    <Input
                      value={bankAccountNumber}
                      onChangeText={setBankAccountNumber}
                      returnKeyType={"next"}
                      getRef={c => (this.bankAccountNumber = c)}
                      blurOnSubmit={false}
                    />
                  </Item>
                  <OTPInput
                    savePass={setPassword}
                    label="Pin"
                    reset={resetPins}
                  />
                  <OTPInput
                    savePass={setRePassword}
                    label="Repeat pin"
                    reset={resetPins}
                  />
                  <LoadingSpinner area="create-account-button">
                    <Button
                      block
                      primary-dark
                      onPress={() => {
                        _ifPwEqualCreateAccount();
                      }}
                      style={{ marginTop: 16 }}
                    >
                      <Text>Create Account</Text>
                    </Button>
                  </LoadingSpinner>
                </Form>
              </Row>
            </Grid>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </Container>
    </SafeAreaView>
  );
}

SignUpScreen.navigationOptions = ({ navigation }) => {
  return {
    header: () => (
      <Header>
        <Left>
          <Button
            transparent
            onPress={() => navigation.dispatch(NavigationActions.back())}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Sign up</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};
