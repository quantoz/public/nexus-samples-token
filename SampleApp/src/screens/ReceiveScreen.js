import React, { useState } from "react";
import QRCode from "react-native-qrcode-svg";
import { SafeAreaView, StyleSheet } from "react-native";
import {
  Container,
  Content,
  Header,
  Button,
  Left,
  Body,
  Icon,
  Right,
  Title,
  View,
  Text
} from "native-base";

export default function ReceiveScreen({ navigation }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content
          padder
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "center"
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 48
            }}
          >
            <Text notification-light>My address</Text>
            <View style={{ marginVertical: 24 }}>
              <QRCode
                value={navigation.getParam("user").keypair.publicKey}
                size={200}
              />
            </View>
            <Text body-light style={{ width: 200, textAlign: "center" }}>
              Ask the sender to scan this QR code to easily send you tokens.
            </Text>
          </View>
          <Text label-bold-light>Address</Text>
          <Text body-light>
            {navigation
              .getParam("user")
              .keypair.publicKey.replace(/[^\dA-Z]/g, "")
              .replace(/(.{4})/g, "$1 ")
              .trim()}
          </Text>
        </Content>
      </Container>
    </SafeAreaView>
  );
}

ReceiveScreen.navigationOptions = ({ navigation }) => ({
  header: () => (
    <Header darkTheme>
      <Left>
        <Button transparent onPress={() => navigation.goBack()}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Title>Receive</Title>
      </Body>
      <Right />
    </Header>
  )
});
