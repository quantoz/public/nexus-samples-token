import React from "react";
import { shallow, mount } from "enzyme";
import LoginScreen from "./LogInScreen";

describe("Testing LoginScreen", () => {
  test("Check inputbox", () => {
    const wrapper = mount(<LoginScreen />);
    const email = wrapper
      .findWhere(node => node.prop("testID") === "login-mail")
      .first();
    expect(email).toBeTruthy();

    email.props().onChangeText("bob@mail.com");
  });
});
