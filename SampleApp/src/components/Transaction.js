import React, { useState, useEffect } from "react";
import { StyleSheet, Animated, Easing } from "react-native";
import { ListItem, Left, Icon, Button, Body, Text, Right } from "native-base";
import Config from "../Config";

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "#fff",
    paddingRight: 0,
    height: 72
  },
  noLeftMargin: {
    marginLeft: 0
  },
  icon: {
    color: "#02182B"
  }
});

export default Transaction = props => {
  const [loadingIcon] = useState(new Animated.Value(0));
  const MAX_CHAR_MEMO = 20;

  // Second interpolate beginning and end values (in this case 0 and 1)
  const spin = loadingIcon.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"]
  });

  truncateLongMemo = (str, n) => {
    return str.length > n ? str.substr(0, n - 1) + "..." : str;
  };

  reduceAddress = addr => {
    return addr.substring(0, 4) + "..." + addr.slice(addr.length - 4);
  };

  useEffect(() => {
    Animated.loop(
      Animated.timing(loadingIcon, {
        toValue: 1,
        duration: 1000,
        easing: Easing.easeInOut,
        useNativeDriver: true
      })
    ).start();
  }, []);

  const _renderLoader = () => {
    if (props.loader) {
      return (
        <Animated.View style={{ transform: [{ rotate: spin }] }}>
          <Icon ios="refresh-outline" android="md-refresh" />
        </Animated.View>
      );
    }
  };
  return (
    <ListItem noIndent icon style={styles.listItem}>
      <Left>
        <Button style={{ backgroundColor: "#EBF6FB" }}>
          {props.transactionType === "incoming" ? (
            <Icon name="arrow-down-circle-outline" style={styles.icon} />
          ) : (
            <Icon name="arrow-up-circle-outline" style={styles.icon} />
          )}
        </Button>
      </Left>
      <Body>
        <Text label-light>
          {props.memo !== undefined
            ? truncateLongMemo(props.memo, MAX_CHAR_MEMO)
            : reduceAddress(props.address)}
        </Text>
        <Text secondary-light>{props.date}</Text>
        {_renderLoader(props.loader)}
      </Body>
      <Right>
        <Text
          label-bold-light
          incomingAmount={props.transactionType === "incoming"}
        >
          {props.transactionType === "incoming" ? "+ " : "- "}
          {props.amount &&
            (typeof props.amount != "string"
              ? (+props.amount).toFixed(2)
              : props.amount)}
        </Text>
      </Right>
    </ListItem>
  );
};
