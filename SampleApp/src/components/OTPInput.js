import React, { useEffect, useState } from "react";
import { Label, Input, Item } from "native-base";
import { StyleSheet } from "react-native";
import { Col, Grid } from "react-native-easy-grid";

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  noLeftMargin: {
    marginLeft: 0,
    marginRight: 16
  }
});

const OTPInput = (props, ref) => {
  const [firstDigit, setFirstDigit] = useState("");
  const [secondDigit, setSecondDigit] = useState("");
  const [thirdDigit, setThirdDigit] = useState("");
  const [fourthDigit, setFourthDigit] = useState("");
  const [fifthDigit, setFifthDigit] = useState("");

  const _numberOnlyRegex = /^\d+$/;

  useEffect(() => {
    props.savePass(
      firstDigit + secondDigit + thirdDigit + fourthDigit + fifthDigit
    );
  }, [firstDigit, secondDigit, thirdDigit, fourthDigit, fifthDigit]);

  useEffect(() => {
    if (props.reset) {
      resetPinFields();
    }
  }, [props.reset]);

  const resetPinFields = () => {
    setFirstDigit("");
    setSecondDigit("");
    setThirdDigit("");
    setFourthDigit("");
    setFifthDigit("");
  };

  return (
    <>
      <Label style={{ marginTop: 24, color: "#fff" }}>{props.label}</Label>
      <Grid style={{ marginBottom: 16 }}>
        <Col>
          <Item style={styles.noLeftMargin}>
            <Input
              value={firstDigit}
              onChangeText={value => {
                if (
                  _numberOnlyRegex.test(value) === true &&
                  value.length === 1
                ) {
                  setFirstDigit(value);
                  this[`secondDigit_${props.label}`]._root.focus();
                } else {
                  setFirstDigit(firstDigit.replace(_numberOnlyRegex, ""));
                }
              }}
              returnKeyType={"next"}
              keyboardType="number-pad"
              textAlign="center"
              maxLength={1}
              blurOnSubmit={false}
              secureTextEntry={true}
            />
          </Item>
        </Col>
        <Col>
          <Item style={styles.noLeftMargin}>
            <Input
              value={secondDigit}
              onChangeText={value => {
                if (
                  _numberOnlyRegex.test(value) === true &&
                  value.length === 1
                ) {
                  setSecondDigit(value);
                  this[`thirdDigit_${props.label}`]._root.focus();
                } else {
                  setSecondDigit(secondDigit.replace(_numberOnlyRegex, ""));
                }
              }}
              returnKeyType={"next"}
              ref={c => (this[`secondDigit_${props.label}`] = c)}
              keyboardType="number-pad"
              textAlign="center"
              maxLength={1}
              secureTextEntry={true}
            />
          </Item>
        </Col>
        <Col>
          <Item style={styles.noLeftMargin}>
            <Input
              value={thirdDigit}
              onChangeText={value => {
                if (
                  _numberOnlyRegex.test(value) === true &&
                  value.length === 1
                ) {
                  setThirdDigit(value);
                  this[`fourthDigit_${props.label}`]._root.focus();
                } else {
                  setThirdDigit(thirdDigit.replace(_numberOnlyRegex, ""));
                }
              }}
              returnKeyType={"next"}
              ref={c => (this[`thirdDigit_${props.label}`] = c)}
              keyboardType="number-pad"
              textAlign="center"
              maxLength={1}
              secureTextEntry={true}
            />
          </Item>
        </Col>
        <Col>
          <Item style={styles.noLeftMargin}>
            <Input
              value={fourthDigit}
              onChangeText={value => {
                if (
                  _numberOnlyRegex.test(value) === true &&
                  value.length === 1
                ) {
                  setFourthDigit(value);
                  this[`fifthDigit_${props.label}`]._root.focus();
                } else {
                  setFourthDigit(fourthDigit.replace(_numberOnlyRegex, ""));
                }
              }}
              returnKeyType={"next"}
              ref={c => (this[`fourthDigit_${props.label}`] = c)}
              keyboardType="number-pad"
              textAlign="center"
              maxLength={1}
              secureTextEntry={true}
            />
          </Item>
        </Col>
        <Col>
          <Item style={styles.noLeftMargin}>
            <Input
              value={fifthDigit}
              onChangeText={value => {
                if (
                  _numberOnlyRegex.test(value) === true &&
                  value.length === 1
                ) {
                  setFifthDigit(value);
                } else {
                  setFifthDigit(fifthDigit.replace(_numberOnlyRegex, ""));
                }
              }}
              returnKeyType={"next"}
              ref={c => (this[`fifthDigit_${props.label}`] = c)}
              keyboardType="number-pad"
              textAlign="center"
              maxLength={1}
              secureTextEntry={true}
            />
          </Item>
        </Col>
      </Grid>
    </>
  );
};

export default OTPInput;
