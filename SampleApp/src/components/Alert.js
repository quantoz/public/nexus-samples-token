import { Alert } from "react-native";

export const InfoModal = infoTopic => {
  var info = "-";
  var title = "-";

  if (infoTopic === "password") {
    title = "Info";
    info =
      "You password must consist of at least 8 characters, of which 1 capital, 1 numeric and 1 special character.";
  }

  if (infoTopic === "disclaimer") {
    title = "Disclaimer & Privacy";
    info =
      "Privacy Policy\n\n" +
      "1. Information collection\n" +
      "When using the sample app, we ask certain information from you: " +
      "*Personal Information*: We dont collect any personal data, except the email addresses and information you submitted voluntarily.\n" +
      "2. Information usage\n" +
      "Your information will not be shared with others and is only used internally for the following purposes: " +
      "to provide our services or information you request, and to process and complete any transactions; " +
      "to send you confirmations, updates, security alerts, and support and administrative messages and otherwise facilitate your use of, and our administration and operation of, our services;" +
      "\n\n​" +
      "Disclaimer\n" +
      "​You acknowledge and agree in relation to any downloads which are accessed on or via the Quantoz website (“Download”) " +
      "that (i) you have a right of license to such Download, you do not own any intellectual property rights in such Download " +
      "and your use of such Download may be subject to an applicable end user licence agreement; (ii) you are not permitted to " +
      "modify, copy, commercially exploit, re­sell or otherwise use a Download in any way which is contrary to applicable laws or regulations." +
      "\n\n​" +
      "Without limiting the foregoing, you expressly agree that Quantoz, its officers, subsidiaries, employees and/or affiliates " +
      "shall have no liability whatsoever for the performance of any software product you purchase and/or download through, or via " +
      "a link from, the Quantoz website (“Download”). You expressly acknowledge and agree that use of any Download is at your sole " +
      "risk and that the entire risk as to satisfactory quality, performance, accuracy and effort is with you. To the maximum extent " +
      "permitted by applicable law, Downloads are provided “As Is” and without warranty of any kind, and Quantoz and its group " +
      "companies hereby disclaim all warranties and conditions with respect to the Download, either express, implied or statutory, " +
      "including (without limitation) the implied warranties and/or conditions of merchantability, satisfactory quality, fitness for " +
      "a particular purpose, accuracy, quiet enjoyment and non­infringement of third­party rights. Quantoz does not warrant against " +
      "interference with your enjoyment of the Download, that the functions contained in the Download will be continuous, uninterrupted, " +
      "secure, virus­free, or error­free, or that defects in the Download will be corrected. Should the Download prove defective, you " +
      "assume the entire cost of all necessary servicing, repair or correction.";
  }

  Alert.alert(
    title,
    info,
    [
      {
        text: "OK"
      }
    ],
    { cancelable: false }
  );
};

export const ErrorModal = (textOrErrorObject, okFun) => {
  var title = "-";
  var error = "-";
  var errorType = typeof textOrErrorObject;

  if (errorType === "string") {
    title = "Error";
    error = textOrErrorObject;
  } else if (typeof textOrErrorObject.response === "object") {
    if (typeof textOrErrorObject.response.data === "object") {
      var responseBody = textOrErrorObject.response.data;
      if (typeof responseBody.error === "string") {
        // authorization error
        title = responseBody.error;
        error = responseBody.error_description;
      } else if (typeof responseBody.errors === "object") {
        // TokenSampleAPI error
        title = responseBody.title;
        error = JSON.stringify(responseBody.errors, null, 2);
      } else if (typeof responseBody.detail === "string") {
        title = responseBody.title;
        error = responseBody.detail;
      } else {
        error = JSON.stringify(responseBody, null, 2);
      }
    } else if (typeof textOrErrorObject.response.data === "string") {
      title = textOrErrorObject.message;
      error = textOrErrorObject.response.data;
    } else {
      error = JSON.stringify(textOrErrorObject, null, 2);
    }
  } else if (typeof textOrErrorObject.message === "string") {
    title = "Error";
    error = textOrErrorObject.message;
  } else {
    error = JSON.stringify(textOrErrorObject, null, 2);
  }

  if (error === "Network Error") {
    error = "API not reacheable - connection available?";
  }

  // okFun is the function passed to this Alert that contains the function executed
  // upon the Alerts box gets clicked away
  if (okFun) {
    title = "Hi!";
  }

  Alert.alert(
    title,
    error,
    [
      {
        text: "OK",
        onPress: okFun
      }
    ],
    { cancelable: false }
  );
};
