// @flow

import variable from "./../variables/platform";

export default (variables /* : * */ = variable) => {
  const spinnerTheme = {
    height: 48,
    minWidth: 160
  };

  return spinnerTheme;
};
