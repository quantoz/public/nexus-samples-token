// @flow

export default () => {
  const theme = {
    "NativeBase.Item": {
      ".fixedLabel": {
        "NativeBase.Label": {
          paddingLeft: null
        },
        marginLeft: 15
      },
      ".inlineLabel": {
        "NativeBase.Label": {
          paddingLeft: null
        },
        marginLeft: 15
      },
      ".placeholderLabel": {
        "NativeBase.Input": {}
      },
      ".stackedLabel": {
        "NativeBase.Label": {
          top: 5,
          paddingLeft: null
        },
        "NativeBase.Input": {
          paddingLeft: null,
          marginLeft: null
        },
        "NativeBase.Icon": {
          marginTop: 36
        },
        marginLeft: 15
      },
      ".floatingLabel": {
        "NativeBase.Input": {
          // where the user needs to click to enter data in an input field
          paddingLeft: null,
          marginLeft: null,
          marginTop: 8
        },
        "NativeBase.Label": {
          // in the floatingLabel case, position of both label and input as a group
          left: 0,
          top: 10
        },
        "NativeBase.Icon": {
          top: 6
        },
        marginTop: 16,
        marginLeft: 16
      },
      ".regular": {
        "NativeBase.Label": {
          left: 0,
          top: 0
        },
        marginLeft: 0
      },
      ".rounded": {
        "NativeBase.Label": {
          left: 0
        },
        marginLeft: 0
      },
      ".underline": {
        "NativeBase.Label": {
          left: 0,
          top: 0,
          position: "relative"
        },
        "NativeBase.Input": {
          left: -15
        },
        marginLeft: 15
      },
      ".last": {
        marginBottom: 16,
        paddingLeft: 0,
        marginLeft: 0
      },
      "NativeBase.Label": {
        paddingRight: 5
      },
      marginLeft: 15
    }
  };

  return theme;
};
