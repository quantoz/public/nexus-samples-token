// @flow

import variable from "./../variables/platform";

export default (variables /* : * */ = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    ".note": {
      color: "#a7a7a7",
      fontSize: variables.noteFontSize
    },
    ".balance-dark": {
      fontFamily: "Nunito-SemiBold",
      fontSize: 32,
      lineHeight: 40,
      letterSpacing: -0.2,
      fontWeight: "700",
      color: variables.whiteColor
    },
    ".notification-dark": {
      fontFamily: "Nunito-SemiBold",
      fontSize: 24,
      fontWeight: "700",
      color: variables.whiteColor
    },
    ".notification-light": {
      fontFamily: "Nunito-SemiBold",
      fontSize: 24,
      fontWeight: "700",
      color: variables.nileBlueColor
    },
    ".button": {
      fontFamily: "Nunito-Bold",
      fontSize: 17,
      lineHeight: 17,
      letterSpacing: -0.4,
      fontWeight: "700"
    },
    ".body-bold-light": {
      fontFamily: "Nunito-SemiBold",
      fontSize: 17,
      lineHeight: 24,
      fontWeight: "700"
    },
    ".body-light": {
      fontFamily: "Nunito-Regular",
      fontSize: 17,
      lineHeight: 24,
      fontWeight: "400",
      color: variables.nileBlueColor
    },
    ".label-bold-light": {
      fontFamily: "Nunito-Bold",
      fontSize: 17,
      lineHeight: 20,
      fontWeight: "700",
      color: variables.nileBlueColor
    },
    ".label-bold-dark": {
      fontFamily: "Nunito-Bold",
      fontSize: 17,
      lineHeight: 20,
      fontWeight: "700",
      color: variables.polarColor
    },
    ".label-light": {
      fontFamily: "Nunito-Regular",
      fontSize: 17,
      lineHeight: 20,
      letterSpacing: -0.5,
      fontWeight: "400",
      color: variables.nileBlueColor
    },
    ".secondary-light": {
      fontFamily: "Nunito-SemiBold",
      fontSize: 13,
      lineHeight: 16,
      letterSpacing: -0.04,
      fontWeight: "600",
      color: variables.submarineColor
    }
  };

  return textTheme;
};
