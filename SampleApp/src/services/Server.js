import React from "react";
import Config from "Config";
import axios from "axios";

const convertToCamelCaseAuthData = json_auth_data => {
  return {
    accessToken: json_auth_data.access_token,
    expiresIn: json_auth_data.expires_in,
    tokenType: json_auth_data.token_type,
    refreshToken: json_auth_data.refresh_token,
    scope: json_auth_data.scope
  };
};

const convertToSearchParams = params => {
  return Object.keys(params)
    .map(key => {
      return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
    })
    .join("&");
};

export const authorize = async (config, { user, password }) => {
  var params = [];
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;
  params["grant_type"] = config.grantType;
  params["scope"] = config.scopes.join(" ");
  params["username"] = user;
  params["password"] = password;

  var requestOptions = {
    url: config.serviceConfiguration.tokenEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    data: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return convertToCamelCaseAuthData(response.data);
};

export const refresh = async (config, { refreshToken }) => {
  var params = [];
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;
  params["grant_type"] = config.grantType;
  params["refresh_token"] = refreshToken;
  params["scope"] = config.scopes.join(" ");

  var requestOptions = {
    url: config.serviceConfiguration.tokenEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    data: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return convertToCamelCaseAuthData(response.data);
};

export const revoke = async (config, { tokenToRevoke }) => {
  var params = [];
  params["token"] = tokenToRevoke;
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;

  var requestOptions = {
    url: config.serviceConfiguration.revocationEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// registerRequest:
// {
// 	"email": "jansen@8.nl",
// 	"password": "Pass123$",
// 	"cryptoAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP"
// }
export const registerUser = async registerRequest => {
  let requestOptions = {
    url: "/customers",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    data: JSON.stringify({
      ...registerRequest,
      AnonymousRequestToken: Config.acceptedAnonymousRequestToken
    }),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

export const submitTransactionAsync = async (user, tx) => {
  let requestOptions = {
    url: "/transactions/submit",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + user.token.accessToken
    },
    data: JSON.stringify({
      transactionEnvelope: tx
    }),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// createPaymentRequest:
// {
// 	"fromAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
// 	"toAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
//  "tokenCode": <DEFAULT_ASSET>,
//  "amount": "2.93",
//  "memo": "1 bread"
// }
export const createPayment = async (user, createPaymentRequest) => {
  let requestOptions = {
    url: "/transactions/payments",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + user.token.accessToken
    },
    data: JSON.stringify(createPaymentRequest),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// createPaymentRequest:
// {
// 	"fromAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
//  "tokenCode": <DEFAULT_ASSET>,
//  "amount": "2.93",
// }
export const createPayout = async (user, createPayoutRequest) => {
  let requestOptions = {
    url: "/transactions/payouts",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + user.token.accessToken
    },
    data: JSON.stringify(createPayoutRequest),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};
