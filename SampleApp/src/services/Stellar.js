import React from "react";
import Config from "Config";
import StellarBase from "stellar-base";
import axios from "axios";

export const getBalanceAsync = async user => {
  let requestOptions = {
    url: "/accounts/" + user.keypair.publicKey,
    baseURL: Config.STELLAR_API,
    method: "get",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  var balances = response.data.balances;

  var default_asset = balances.find(x => x.asset_code === Config.DEFAULT_ASSET);

  return {
    balance: default_asset.balance
  };
};

export const getTransactionsAsync = async user => {
  let requestOptions = {
    url: "/accounts/" + user.keypair.publicKey + "/payments",
    params: {
      limit: 30, // max 30 payments per page (we get 1 page)
      order: "desc" // order desc to get the most recent payments
    },
    baseURL: Config.STELLAR_API,
    method: "get",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  // const unknownTxs = [
  //   {
  //     from: "FROM?",
  //     to: "TO?",
  //     amount: "?",
  //     created: ""
  //   }
  // ];

  var response = await axios.request(requestOptions);
  var records = response.data._embedded.records;
  var filteredTx = records.filter(x => x.type === "payment");
  var mappedTx = await Promise.all(
    filteredTx.map(async x => {
      var tx = await getStellarTransactionAsync(x.transaction_hash);
      return {
        from: x.from,
        to: x.to,
        amount: x.amount,
        created: x.created_at,
        memo: tx.memo
      };
    })
  );

  var sortedTx = mappedTx.sort((x, y) => {
    if (x.created === y.created) {
      return 0;
    }
    return Date.parse(x.created) > Date.parse(y.created) ? -1 : 1;
  });

  return sortedTx;
};

export const getStellarTransactionAsync = async txHash => {
  let requestOptions = {
    url: "/transactions/" + txHash,
    baseURL: Config.STELLAR_API,
    method: "get",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  var response = await axios.request(requestOptions);
  return response.data;
};

export const signTransaction = (envelope, secret) => {
  const kp = StellarBase.Keypair.fromSecret(secret);
  console.log("cfg.txe", envelope);
  console.log("cfg.pfr", Config.networkPassphrase);
  const tx = new StellarBase.Transaction(envelope, Config.networkPassphrase);
  tx.sign(kp);
  return tx.toXDR();
};
