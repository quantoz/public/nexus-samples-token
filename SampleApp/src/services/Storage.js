import AsyncStorage from "@react-native-community/async-storage";

const setItemAsync = async (key, value) => {
  return await AsyncStorage.setItem(key, JSON.stringify(value));
};

const getItemAsync = async key => {
  return await AsyncStorage.getItem(key).then(result => {
    if (result) {
      result = JSON.parse(result);
    }
    return result;
  });
};

const removeItemAsync = async key => {
  return await AsyncStorage.removeItem(key);
};

const selectEmail = userOrMail => {
  if (typeof userOrMail === "string") {
    return userOrMail;
  } else {
    return userOrMail.email;
  }
};

export const getUserAsync = async email => {
  return await getItemAsync(selectEmail(email));
};

export const noStoredUsers = async () => {
  return (await getAllUsersAsync()) == null;
};

const getAllUsersAsync = async () => {
  return await getItemAsync("usersList");
};

const appendUserList = async email => {
  var users = await getAllUsersAsync();
  if (users == null) {
    users = [];
  }
  users.push(selectEmail(email));
  return await setItemAsync("usersList", users);
};

const deleteUserFromUserList = async email => {
  var users = await getAllUsersAsync();
  var index = users.indexOf(selectEmail(email));
  if (index > -1) {
    users.splice(index, 1);
  }
  if (users.length == 0) {
    await removeItemAsync("usersList");
  }
};

export const createUserAsync = async (email, user) => {
  await appendUserList(email);
  return await setItemAsync(selectEmail(email), { isUser: true, ...user });
};

// accepts user object or emailstring
// append user with "email" with supplied `user` object
export const appendUserAsync = async (email, user) => {
  var storedUser = await getUserAsync(email);
  if (storedUser == null) {
    return await createUserAsync(email, user);
  } else {
    return await setItemAsync(selectEmail(email), {
      isUser: true,
      ...storedUser,
      ...user
    });
  }
};

// accepts user object or emailstring
export const setUserItemAsync = async (email, key, value) => {
  var user = await getUserAsync(selectEmail(email));
  if (user == null) {
    user = {};
    await createUserAsync(email, user);
  }
  user[key] = value;
  return await setItemAsync(selectEmail(email), user);
};

// accepts user object or emailstring
export const deleteUserItemAsync = async (email, key) => {
  var user = await getUserAsync(selectEmail(email));
  if (user == null) {
    return;
  }
  delete user[key];
  return await setItemAsync(selectEmail(email), user);
};

// accepts user object or emailstring
export const deleteUserAsync = async email => {
  return await removeItemAsync(selectEmail(email));
};

// accepts user object or emailstring
export const setUserTokenAsync = async (email, token) => {
  var user = await getUserAsync(selectEmail(email));
  user["token"] = token;
  return await setItemAsync(selectEmail(email), user);
};

// accepts user object or emailstring
export const deleteUserTokenAsync = async email => {
  var user = await getUserAsync(selectEmail(email));
  delete user["token"];
  return await setItemAsync(selectEmail(email), user);
};

export const getLoggedInUserEmailAsync = async () => {
  return await getItemAsync("loggedInUser");
};

export const isUserLoggedInAsync = async () => {
  var mem = await getLoggedInUserEmailAsync();
  return typeof mem === "string";
};

export const setUserAsLoggedInAsync = async email => {
  return await setItemAsync("loggedInUser", selectEmail(email));
};

export const clearUserLoggedInStatusAsync = async () => {
  return await removeItemAsync("loggedInUser");
};
