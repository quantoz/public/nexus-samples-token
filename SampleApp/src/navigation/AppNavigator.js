import React, { useState, useEffect } from "react";
import { Platform } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

// import screens and components
import MainStack from "./MainNavigator";
import LogInScreen from "_screens/LogInScreen";
import SignUpScreen from "_screens/SignUpScreen";
import ConfirmationScreen from "_screens/ConfirmationScreen";
import AuthLoadingScreen from "_screens/AuthLoadingScreen";
import ScanScreen from "_screens/ScanScreen";

const SignUpStack = createStackNavigator(
  {
    SignUpScreen,
    SignUpConfirmed: {
      screen: ConfirmationScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "SignUpScreen"
  }
);

const AuthStack = createStackNavigator(
  {
    LogInScreen,
    SignUp: SignUpStack,
    ViewerScanScreen: {
      screen: ScanScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#02182B"
      },
      headerTintColor: "#FFF",
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 17,
        lineHeight: 36,
        letterSpacing: -0.04
      }
    },
    initialRouteName: "LogInScreen",
    header: null,
    headerMode: "none"
  }
);

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStack,
    App: MainStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
