export default {
  SAMPLE_API: "http://10.0.2.2:5000/api",
  networkPassphrase: "Test SDF Network ; September 2015",
  STELLAR_API: "https://horizon-testnet.stellar.org",
  DEFAULT_ASSET: "CEUR",
  decimalPrecision: 2,
  acceptedAnonymousRequestToken: "<FILL IN TOKEN USED IN API>"
};

const issuer = "http://10.0.2.2:5000";
export const authConfig = {
  serviceConfiguration: {
    authorizationEndpoint: `${issuer}/connect/authorize`,
    tokenEndpoint: `${issuer}/connect/token`,
    revocationEndpoint: `${issuer}/connect/revocation`
  },
  clientId: "client",
  clientSecret: "511536EF-F270-4058-80CA-1C89C192F69A",
  scopes: ["IdentityServerApi", "offline_access"],
  dangerouslyAllowInsecureHttpRequests: true,
  grantType: "password",
  redirectUrl: "com.quantoznexus.devidentity://oauthredirect"
};
