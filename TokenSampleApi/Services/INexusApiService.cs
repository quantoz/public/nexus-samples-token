﻿using System.Net.Http;
using System.Threading.Tasks;
using TokenSampleApi.Data.DTOs.Repo.Customers;
using TokenSampleApi.Data.DTOs.Services.NexusApiService;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Account;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Customer;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment;

namespace TokenSampleApi.Services
{
    public interface INexusApiService
    {
        public Task<PagedResult<GetPaymentResponse>> GetPayments(int page = 1, int limit = 50);
        public Task<GetCustomerResponse> GetCustomer(string customerCode);
        public Task<PagedResult<GetPaymentResponse>> GetCustomerPayments(string customerCode, int page = 1, int limit = 50);
        public Task<PostCustomerResponse> CreateCustomer(string customerCode, string email, string pubKey, string firstName, string lastName, string bankAccountNumber);
        public Task<GetAccountResponse> GetAccount(string accountCode);
        public Task<PagedResult<GetPaymentResponse>> GetAccountPayments(string accountCode, int page = 1, int limit = 50);
        public Task<PostAccountResponse> CreateAccount(string customerCode);
        public Task<PostPaymentsResponse> CreatePayment(PostPaymentOutputs[] paymentOutputs, string memo = null);
        public Task<PostPaymentsResponse> CreateFunding(string customerCode, string accountCode, string tokenCode, decimal amount, string memo = null);
        public Task<PostPaymentsResponse> CreatePayout(string accountCode, string tokenCode, decimal amount, string memo = null);
        public Task<HttpResponseMessage> Submit(string transactionEnvelope);
    }
}
