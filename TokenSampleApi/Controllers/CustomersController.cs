﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TokenSampleApi.Data.DTOs.Repo.Customers;
using TokenSampleApi.Logic.Interfaces;
using static IdentityServer4.IdentityServerConstants;
using TokenSampleApi.Data.DTOs.Controller.Customer;
using TokenSampleApi.Data.DTOs.Logic.Customers;
using Microsoft.Extensions.Configuration;

namespace TokenSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : BaseController
    {
        private readonly ICustomerLogic _customerLogic;

        private readonly string _acceptedAnonymousRequestToken;

        public CustomersController(
            IConfiguration configuration,
            ICustomerLogic customerLogic)
            : base()
        {
            _acceptedAnonymousRequestToken = configuration.GetValue<string>("accepted-anonymous-request-token");
            _customerLogic = customerLogic;
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetCustomerDto>> GetCustomer([FromRoute] int id)
        {
            var customer = await _customerLogic.Get(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(new GetCustomerDto { Email = customer.Email });
        }

        // POST:
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<ConfirmCustomerDto>> CreateCustomer([FromBody] CreateCustomerDto dto)
        {
            if (dto.AnonymousRequestToken != _acceptedAnonymousRequestToken)
            {
                return BadRequest("Incorrect Anonymous Request Token");
            }

            if (string.IsNullOrWhiteSpace(dto.Password))
            {
                return BadRequest("Password is not filled in");
            }

            try
            {
                return CreatedAtAction("CreateCustomer", await _customerLogic.Create(dto));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (HttpRequestException e)
            {
                return Problem(e.Message);
            }
        }
    }
}
