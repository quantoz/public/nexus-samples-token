﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TokenSampleApi.Data;
using TokenSampleApi.Data.DTOs.Controller.Transaction;
using TokenSampleApi.Data.DTOs.Logic.Transaction;
using TokenSampleApi.Data.Pocos;
using TokenSampleApi.Helpers;
using TokenSampleApi.Logic.Interfaces;
using TokenSampleApi.Models;
using static IdentityServer4.IdentityServerConstants;

namespace TokenSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ITransactionLogic _transactionLogic;

        public TransactionsController(
            UserManager<ApplicationUser> userManager,
            ITransactionLogic transactionLogic)
            : base()
        {
            _userManager = userManager;
            _transactionLogic = transactionLogic;
        }

        // POST: api/transactions/payments
        [HttpPost("payments")]
        public async Task<ActionResult<TransactionEnvelopeDto>> CreatePayment([FromBody] PostCreatePaymentDto dto)
        {
            var clientMail = (await _userManager.GetUserAsync(User)).UserName;
            var customerCode = Tools.TransformEmailToText(clientMail);

            var payment = new CreatePaymentDto {
                CustomerCode = customerCode,
                FromAddress = dto.FromAddress,
                ToAddress = dto.ToAddress,
                Amount = Convert.ToDecimal(dto.Amount),
                TokenCode = dto.TokenCode,
                Memo = dto.Memo
            };

            var txDto = await _transactionLogic.CreatePayment(payment);

            return Ok(txDto);
        }

        // POST: api/transactions/payments
        [HttpPost("payouts")]
        public async Task<ActionResult<TransactionEnvelopeDto>> CreatePayout([FromBody] PostCreatePayoutDto dto)
        {
            var clientMail = (await _userManager.GetUserAsync(User)).UserName;
            var customerCode = Tools.TransformEmailToText(clientMail);

            var payout = new CreatePayoutDto
            {
                CustomerCode = customerCode,
                FromAddress = dto.FromAddress,
                Amount = Convert.ToDecimal(dto.Amount),
                TokenCode = dto.TokenCode,
                Memo = dto.Memo
            };

            var txDto = await _transactionLogic.CreatePayout(payout);

            return Ok(txDto);
        }


        // POST: api/transactions/submit
        [HttpPost("submit")]
        public async Task<ActionResult> SubmitTransaction([FromBody] SubmitTransactionDto submitTransactionDto)
        {
            var result = await _transactionLogic.SubmitTransaction(new TransactionEnvelopeDto(submitTransactionDto.TransactionEnvelope));
            var reason = await result.Content.ReadAsStringAsync();

            return StatusCode((int)result.StatusCode, reason);
        }
    }
}
