﻿using System;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Account;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Customer
{
    public class PostCustomerResponse
    {
        public string CustomerCode { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }
        public string Trustlevel { get; set; }
        public string CurrencyCode { get; set; }
        public PostAccountResponse[] Accounts { get; set; }
    }
}
