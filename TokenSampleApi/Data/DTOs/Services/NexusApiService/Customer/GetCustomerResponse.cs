﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Customer
{
    public class GetCustomerResponse
    {
        public string CustomerCode { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public string BankAccount { get; set; }
        public string Status { get; set; }
        public string Trustlevel { get; set; }
        public string PortFolioCode { get; set; }
        public bool IsHighRisk { get; set; }
    }
}
