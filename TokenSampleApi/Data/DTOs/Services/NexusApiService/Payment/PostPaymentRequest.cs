﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment
{
    public class PostPaymentsRequest
    {

        /// <summary>
        /// Callback url, required to receive notifications on status updates.
        /// </summary>
        public string CallbackUrl { get; set; }

        /// <summary>
        /// This value will be put in the Memo field of a payment transaction
        /// </summary>
        [StringLength(28)]
        public string Memo { get; set; }

        /// <summary>
        /// Transaction Envelope result is only valid for ExpireSeconds seconds
        /// </summary>
        public int? ExpireSeconds { get; set; }

        /// <summary>
        /// Payment destinations
        /// </summary>
        [Required]
        public PostPaymentOutputs[] Payments { get; set; }
    }

    public class PostPaymentOutputs
    {
        /// <summary>
        /// Crypto Address of the sender of the tokens.
        /// </summary>
        [Required]
        public string Sender { get; set; }

        /// <summary>
        /// Crypto Address of the receiver of the tokens.
        /// </summary>
        [Required]
        public string Receiver { get; set; }

        /// <summary>
        /// Token code of the token to be sent to the Address.
        /// </summary>
        [Required]
        public string TokenCode { get; set; }

        /// <summary>
        /// Amount of Token to be sent to the Address.
        /// </summary>
        [Required]
        public decimal Amount { get; set; }
    }
}
