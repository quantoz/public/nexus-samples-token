﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment
{
    public class PostPayoutRequest
    {
        public string AccountCode { get; set; }
        public string PaymentMethodCode { get; set; }
        public string CallbackUrl { get; set; }
        public string Memo { get; set; }
        public int? ExpireSeconds { get; set; }
        public string TokenCode { get; set; }
        public decimal Amount { get; set; }
        public IDictionary<string, string> Data { get; set; }
    }
}
