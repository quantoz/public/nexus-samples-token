﻿using System;
using System.Collections.Generic;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Account;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment
{
    public class GetPaymentResponse
    {
        public string Code { get; set; }
        public IDictionary<string, string> Data { get; set; }
        public string Hash { get; set; }
        public AccountInfo SenderAccount { get; set; }
        public AccountInfo ReceiverAccount { get; set; }
        public decimal Amount { get; set; }
        public string TokenCode { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
    }
}
