﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment
{
    public class PostFundingRequest
    {
        public string CustomerCode { get; set; }
        public string AccountCode { get; set; }
        public decimal Amount { get; set; }
        public string TokenCode { get; set; }
        public string CallbackUrl { get; set; }
        public string Memo { get; set; }
        public IDictionary<string, string> Data { get; set; }
        public string PaymentMethodCode { get; set; }
    }
}
