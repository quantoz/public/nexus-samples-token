﻿using System;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.TokenSettings;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.TransactionEnvelope;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Account
{
    public class PostAccountResponse
    {
        public string Guid { get; set; }
        public string AccountCode { get; set; }
        public string CustomerCode { get; set; }
        public DateTime Created { get; set; }
        public string DCReceiveAddress { get; set; }
        public string CustomerCryptoAddress { get; set; }
        public string DCCode { get; set; }
        public string AccountStatus { get; set; }
        public TokenSettingsResponse TokenSettings { get; set; }
        public TransactionEnvelopeResponse TransactionEnvelope { get; set; }
    }
}
