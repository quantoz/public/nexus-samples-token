﻿using TokenSampleApi.Data.DTOs.Services.NexusApiService.TokenSettings;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Account
{
    public class PostAccountRequest
    {
        public string AccountType { get; set; }
        public string CryptoCode { get; set; }
        public string CustomerCryptoAddress { get; set; }
        public TokenSettingsRequest TokenSettings { get; set; }
    }

    public class PostBankAccountRequest
    {
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
    }
}
