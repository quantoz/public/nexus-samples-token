﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.TransactionEnvelope
{
    public class TransactionEnvelopeResponse
    {
        public string Hash { get; set; }
        public bool SigningNeeded { get; set; }
        public string SignedTransactionEnvelope { get; set; }
        public string TransactionEnvelopeStatus { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}
