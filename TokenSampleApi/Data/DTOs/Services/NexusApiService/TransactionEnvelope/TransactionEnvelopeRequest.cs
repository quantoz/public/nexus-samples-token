﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.TransactionEnvelope
{
    public class TransactionEnvelopeRequest
    {
        public string Envelope { get; set; }
        public string PaymentCode { get; set; }
    }
}
