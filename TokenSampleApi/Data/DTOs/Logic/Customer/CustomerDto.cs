﻿using System.ComponentModel.DataAnnotations;

namespace TokenSampleApi.Data.DTOs.Logic.Customers
{
    public class ConfirmCustomerDto
    {
        public string Email { get; set; }
        public string CryptoAddress { get; set; }
        public string TransactionEnvelope { get; set; }
    }
}
