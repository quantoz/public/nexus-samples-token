﻿using TokenSampleApi.Data.DTOs.Controller.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Logic.Transaction
{
    public class CreatePayoutDto
    {
        public string CustomerCode { get; set; }
        public string FromAddress { get; set; }
        public decimal Amount { get; set; }
        public string TokenCode { get; set; }
        public string Memo { get; set; }
    }
}
