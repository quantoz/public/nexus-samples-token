﻿using System.ComponentModel.DataAnnotations;

namespace TokenSampleApi.Data.DTOs.Controller.Customer
{
    public class PostCustomerDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
