﻿namespace TokenSampleApi.Data.DTOs.Controller.Customer
{
    public class GetCustomerDto
    {
        public string Email { get; set; }
    }
}
