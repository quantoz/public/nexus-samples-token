﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Controller.Transaction
{
    public class SubmitTransactionDto
    {
        [Required]
        public string TransactionEnvelope { get; set; }
    }
}
