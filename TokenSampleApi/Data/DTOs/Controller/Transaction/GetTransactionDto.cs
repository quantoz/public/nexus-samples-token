﻿using System;
using TokenSampleApi.Data.Pocos;

namespace TokenSampleApi.Data.DTOs.Controller.Transaction
{
    public class GetTransactionDto
    {
        public DateTime Created { get; private set; }
        public Account From { get; set; }
        public Account To { get; set; }
        public decimal Amount { get; private set; }
        public Token Token { get; private set; }

        public GetTransactionDto(TransactionDto transactionDto)
        {
            Created = transactionDto.Created;
            From = transactionDto.From;
            To = transactionDto.To;
            Amount = transactionDto.Amount;
            Token = transactionDto.Token;
        }
    }
}
