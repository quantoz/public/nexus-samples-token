﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Controller.Transaction
{
    public class PostCreatePayoutDto
    {
        [Required]
        public string FromAddress { get; set; }

        [Required]
        public string Amount { get; set; }

        [Required]
        public string TokenCode { get; set; }
        public string Memo { get; set; }
    }
}
