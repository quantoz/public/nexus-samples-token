﻿using System;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment;

namespace TokenSampleApi.Data.Pocos
{
    public class TransactionDto
    {
        public DateTime Created { get; private set; }
        public Account From { get; set; }
        public Account To { get; set; }
        public decimal Amount { get; private set; }
        public Token Token { get; private set; }

        public TransactionDto(GetPaymentResponse paymentResponse)
        {
            Created = paymentResponse.Created;
            From = new Account()
            {
                Address = paymentResponse.SenderAccount?.AccountAddress
            };
            To = new Account()
            {
                Address = paymentResponse.ReceiverAccount?.AccountAddress
            };
            Amount = paymentResponse.Amount;
            Token = new Token(paymentResponse.TokenCode, null);
        }
    }
}
