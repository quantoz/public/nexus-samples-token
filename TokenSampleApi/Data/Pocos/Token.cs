﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.Pocos
{
    public class Token
    {
        public string Code { get; set; }
        public string Issuer { get; set; }
        public Token(string code, string issuer)
        {
            Code = code;
            Issuer = issuer;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var eq = (Token)obj;
            return Code == eq.Code && Issuer == eq.Issuer;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return Code.GetHashCode() ^ Issuer.GetHashCode();
        }
    }
}
