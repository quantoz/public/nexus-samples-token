﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.Pocos
{
    public class TokenBalanceDto
    {
        public Token Token { get; set; }
        public decimal TokenBalance { get; set; }
    }
}
