﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.Pocos
{
    public class PaymentDto
    {
        public decimal AssetAmount { get; set; }
        public string AssetCode { get; set; }
        public string AssetIssuer { get; set; }
        public DateTime CreatedAt { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string PagingToken { get; set; }
        public bool Successful { get; set; }
    }
}
