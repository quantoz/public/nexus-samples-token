﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace TokenSampleApi
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource(IdentityServerConstants.LocalApi.ScopeName)
            };


        public static IEnumerable<Client> Clients(int accessTokenLifetime) =>
            new Client[]
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "Password Client",
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },
                    RedirectUris = { "com.quantoznexus.devidentity://oauthredirect" },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = { IdentityServerConstants.LocalApi.ScopeName },
                    AllowOfflineAccess = true,
                    AccessTokenLifetime = accessTokenLifetime
                }
            };
    }
}