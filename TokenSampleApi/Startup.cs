﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using TokenSampleApi.Data;
using TokenSampleApi.Data.DTOs.Controller.Customer;
using TokenSampleApi.Data.DTOs.Logic.Customers;
using TokenSampleApi.Models;
using TokenSampleApi.Logic.Interfaces;
using TokenSampleApi.Logic;
using TokenSampleApi.Repo;
using TokenSampleApi.Services;
using System;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;

namespace TokenSampleApi
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // configures IIS out-of-proc settings (see https://github.com/aspnet/AspNetCore/issues/14882)
            services.Configure<IISOptions>(iis =>
            {
                iis.AuthenticationDisplayName = "Windows";
                iis.AutomaticAuthentication = false;
            });

            // configures IIS in-proc settings
            services.Configure<IISServerOptions>(iis =>
            {
                iis.AuthenticationDisplayName = "Windows";
                iis.AutomaticAuthentication = false;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));
                //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddHttpClient("HorizonClient");
            services.AddHttpClient("NexusApiClient", c =>
            {
                c.BaseAddress = new Uri(Configuration.GetSection("Nexus").GetValue<string>("nexus-api-url"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(o =>
            {
                // Options to allow the use of a 5-digits pin. For demo purposes only!!.
                // Remove this part and rely on standard security measures.
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 5;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            var accessTokenLifetime = Configuration.GetValue<int>("api-access-token-lifetime");

            var builder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                })
                .AddInMemoryIdentityResources(Config.Ids)
                .AddInMemoryApiResources(Config.Apis)
                .AddInMemoryClients(Config.Clients(accessTokenLifetime))
                .AddAspNetIdentity<ApplicationUser>();

            services.AddScoped<IBlockChainLogic, StellarLogic>();
            services.AddScoped<ITransactionLogic, TransactionLogic>();
            services.AddSingleton<INexusApiService, NexusApiService>();

            services.AddScoped<ICustomerRepo, CustomerRepo>();
            services.AddScoped<ICustomerLogic, CustomerLogic>();

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential();

            // Needed because IdentityServer and API are in 1 project
            services.AddLocalApiAuthentication();
        }

        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDatabaseErrorPage();
                app.UseExceptionHandler("/error-local-development");
            }
            else
            {
                app.UseHttpsRedirection();
                app.UseExceptionHandler("/error");
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseIdentityServer(); // Includes call to UseAuthentication

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}