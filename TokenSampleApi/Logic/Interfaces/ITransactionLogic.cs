﻿using System.Net.Http;
using System.Threading.Tasks;
using TokenSampleApi.Data.DTOs.Logic.Transaction;
using TokenSampleApi.Data.Pocos;

namespace TokenSampleApi.Logic.Interfaces
{
    public interface ITransactionLogic
    {
        public Task<TransactionEnvelopeDto> CreatePayment(CreatePaymentDto paymentDto);
        public Task<TransactionEnvelopeDto> CreatePayout(CreatePayoutDto payoutDto);
        public TransactionEnvelopeDto SignTransaction(TransactionEnvelopeDto transactionEnvelopeDto, string privateKey);
        public Task<HttpResponseMessage> SubmitTransaction(TransactionEnvelopeDto transactionEnvelopeDto);
    }
}
