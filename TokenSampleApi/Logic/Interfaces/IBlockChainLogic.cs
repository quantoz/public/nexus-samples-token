﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TokenSampleApi.Data.Pocos;

namespace TokenSampleApi.Logic.Interfaces
{
    public interface IBlockChainLogic
    {
        Task<string> GetLatestCursor();
        Task<IEnumerable<TokenBalanceDto>> GetAccountBalances(string account);
        Task<TokenBalanceDto> GetAccountBalance(string account, Token token);
        Task<TokenBalanceDto> GetEuroBalance(string account);
        Task<List<PaymentDto>> GetAccountPayments(string account, string? fromCursor = null, string? upToCursor = null);
        string SignTransaction(string transactionEnvelopeBase64, string signer);
    }
}
