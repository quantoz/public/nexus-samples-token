﻿using System.Threading.Tasks;
using TokenSampleApi.Data.DTOs.Logic.Customers;
using TokenSampleApi.Data.DTOs.Repo.Customers;

namespace TokenSampleApi.Repo
{
    public interface ICustomerRepo
    {
        public Task<ConfirmCustomerDto> Create(CreateCustomerDto dto);
        public Task<ConfirmCustomerDto> Get(int customerId);
        public Task<ConfirmCustomerDto> Get(string address);
    }
}
