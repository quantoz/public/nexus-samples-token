﻿using System;
using System.Threading.Tasks;
using TokenSampleApi.Data;
using TokenSampleApi.Data.DTOs.Logic.Customers;
using TokenSampleApi.Data.DTOs.Repo.Customers;

namespace TokenSampleApi.Repo
{
    public class CustomerRepo : ICustomerRepo
    {
        public readonly ApplicationDbContext _context;

        public CustomerRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ConfirmCustomerDto> Create(CreateCustomerDto dto)
        {
            throw new NotImplementedException();
        }

        public Task<ConfirmCustomerDto> Get(int customerId)
        {
            throw new NotImplementedException();
        }

        public Task<ConfirmCustomerDto> Get(string address)
        {
            throw new NotImplementedException();
        }
    }
}
